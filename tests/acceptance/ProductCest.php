<?php


use Page\AbstractPage;
use Page\ProductPage;
use Step\AbstractStep;
use Step\ProductStep;

class ProductCest
{
    protected $faker;
    protected $adminName;
    protected $adminPass;
    protected $productName;
    protected $productNewName;
    protected $productNumber;
    protected $price;
    protected $category;

    public function __construct()
    {
        $this->faker = Faker\Factory::create();
        $this->adminName = "admin";
        $this->adminPass = "admin";
        $this->productName = $this->faker->name;
        $this->productNewName = $this->faker->name;
        $this->productNumber = $this->faker->numerify();
        $this->price = $this->faker->numerify();
    }

    public function product(ProductStep $I, AbstractStep $step){
        $step->login($this->adminName, $this->adminPass);
//        $I->createProduct($this->productName,$this->productNumber,$this->price, AbstractPage::$btnSave,ProductPage::$verifyTextSave);
//        $I->createProduct($this->productName, ($this->productNumber) . "2",
//                          $this->price, AbstractPage::$btnSave_New,ProductPage::$verifyTextSave_New);
        $I->createProduct($this->productName, ($this->productNumber) . "1",$this->price,
                            AbstractPage::$btnSave_Close,ProductPage::$verifyTextSave_Close);
//        $I->editProduct($this->productName,$this->productNewName,AbstractPage::$btnSave,ProductPage::$verifyTextSave);
//        $I->editProduct($this->productName,$this->productNewName,AbstractPage::$btnSave_New,ProductPage::$verifyTextSave_New);
        $I->editProduct($this->productName,$this->productNewName,AbstractPage::$btnSave_Close,ProductPage::$verifyTextSave_Close);
    }
}
