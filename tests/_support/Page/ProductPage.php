<?php


namespace Page;


class ProductPage
{
    public static $urlProductManagement = "/administrator/index.php?option=com_redshop&view=product";
    public static $productManagementText = "Product Management";
    public static $newProductText = "Product: [ New ]";
    public static $productContent = ".content";
    public static $productName = "#product_name";
    public static $productNumber = "#product_number";
    public static $productCategory = "//div[@id='s2id_product_category']//ul[@class='select2-choices']";
    public static $categoryFirstOption = "//div[@id='select2-drop']//li[1]";
    public static $productPrice = "#product_price";
    public static $VAT_Tax = "//div[@id='s2id_product_tax_group_id']";
    public static $VAT_TaxOption = "(//div[@class='select2-result-label'])[2]";
    public static $productImage = "(//input[@type='file'])[6]";
    public static $fileName = "im1.jpeg";
    public static $searchField = "#keyword";
    public static $verifyTextSave_Close = "Product Management";
    public static $verifyTextSave = "Product details saved";
    public static $verifyTextSave_New = "Product: [ New ]";
}