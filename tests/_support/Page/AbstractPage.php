<?php

namespace Page;


class AbstractPage
{
    public static $urlRedShop = "/administrator/index.php?option=com_redshop";
    public static $btnNew = ".button-new";
    public static $btnEdit = ".button-edit";
    public static $btnSave_Close = ".button-save";
    public static $btnSave = ".button-apply";
    public static $btnSave_New = ".button-save-new";
    public static $btnCancel = ".button-cancel";
    public static $btnPublish = ".button-publish";
    public static $btnUnPublish = ".button-unpublish";
    public static $btnCopy = ".button-copy";
    public static $btnDelete = ".button-delete";

    public static $userNameField = "#mod-login-username";

    public static $passwordField = "#mod-login-password";

    public static $btnLogin = ".login-button";

    public static $welcomeRedShopTxt = "Welcome Super User";

    public static $list = "//div[@id='j-main-container']";

    public static $btnClear = ".reset";

    public static $searchTextbox = "#filter_search";

    public static $btnReset = ".reset";

    public static $selectFirstChecbox = "#cb0";
}