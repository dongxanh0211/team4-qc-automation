<?php

namespace Step;


use Page\AbstractPage;
use Page\ProductPage;

class AbstractStep extends \AcceptanceTester
{
    /**
     * @param $userName
     * @param $passWord
     * @throws \Exception
     */
    public function login($userName, $passWord)
    {
        $I = $this;
        $I->amOnPage(AbstractPage::$urlRedShop);
        $I->waitForElementVisible(AbstractPage::$userNameField, 30);
        $I->fillField(AbstractPage::$userNameField, $userName);
        $I->fillField(AbstractPage::$passwordField, $passWord);
        $I->waitForElementVisible(AbstractPage::$btnLogin, 30);
        $I->click(AbstractPage::$btnLogin);
        $I->waitForText(AbstractPage::$welcomeRedShopTxt, 30);
        $I->see(AbstractPage::$welcomeRedShopTxt);
    }

    public function search($searchField, $text)
    {
        $I = $this;
        $I->waitForElementVisible(AbstractPage::$btnClear);
        $I->waitForElementClickable(AbstractPage::$btnClear);
        $I->wait(0.2);
        $I->click(AbstractPage::$btnClear);
        $I->waitForElementVisible($searchField,30);
        $I->fillField($searchField,$text);
        $I->pressKey($searchField,\Facebook\WebDriver\WebDriverKeys::ENTER);
    }

    public function clickButton($button,$title)
    {
        $I = $this;
        switch($button){
            case AbstractPage::$btnSave:
            {
                $I->click(AbstractPage::$btnSave);
                $I->see($title);
                break;
            }
            case AbstractPage::$btnSave_Close:
            {
                $I->click(AbstractPage::$btnSave_Close);
                $I->see($title);
                break;
            }
            case AbstractPage::$btnSave_New:
            {
                $I->click(AbstractPage::$btnSave_New);
                $I->see($title);
                break;
            }
            case AbstractPage::$btnCancel:
            {
                $I->click(AbstractPage::$btnCancel);
                $I->see($title);
                break;
            }
        }
    }
}