<?php


namespace Step;


use Page\AbstractPage;
use Page\ProductPage;
use Step\AbstractStep;


class ProductStep extends \AcceptanceTester
{
    public function createProduct($productName, $productNumber, $price, $btnName, $verifymess)
    {
        $A= new AbstractStep($this->scenario);
        $I = $this;
        $I->amOnPage(ProductPage::$urlProductManagement);
        $I->waitForText(ProductPage::$productManagementText,30);
        $I->waitForElementVisible(AbstractPage::$btnNew,30);
        $I->click(AbstractPage::$btnNew);
        $I->waitForText(ProductPage::$newProductText,30);
        $I->waitForElementVisible(ProductPage::$productContent,30);
        $I->waitForElementVisible(ProductPage::$productName,30);
        $I->fillField(ProductPage::$productName,$productName);
        $I->waitForElementVisible(ProductPage::$productNumber,30);
        $I->fillField(ProductPage::$productNumber,$productNumber);
        $I->waitForElementVisible(ProductPage::$productPrice,30);
        $I->fillField(ProductPage::$productPrice,$price);
        $I->waitForElementClickable(ProductPage::$VAT_Tax,30);
        $I->click(ProductPage::$VAT_Tax);
        $I->click(ProductPage::$VAT_TaxOption);
        $I->waitForElementVisible(ProductPage::$productCategory,30);
        $I->click(ProductPage::$productCategory);
        $I->click(ProductPage::$categoryFirstOption);
        $I->attachFile(ProductPage::$productImage,ProductPage::$fileName);
        $A->clickButton($btnName, $verifymess);
    }

    public function editProduct($oldName, $newProduct, $btnName, $verifytext)
    {
        $A= new AbstractStep($this->scenario);
        $I = $this;
        $I->amOnPage(ProductPage::$urlProductManagement);
        $A->search(ProductPage::$searchField,$oldName);
        $I->waitForElementVisible(AbstractPage::$selectFirstChecbox);
        $I->click(AbstractPage::$selectFirstChecbox);
        $I->waitForElementVisible(AbstractPage::$btnEdit,30);
        $I->click(AbstractPage::$btnEdit);
        $I->waitForElementVisible(ProductPage::$productName,30);
        $I->fillField(ProductPage::$productName,$newProduct);
        $A->clickButton($btnName,$verifytext);
    }

}